package io.gnn.keymaker;

import android.app.Application;

import hugo.weaving.DebugLog;

@DebugLog
public class KeyMakerApplication extends Application {
    private static KeyMakerApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public static synchronized KeyMakerApplication getInstance() {
        return instance;
    }
}
