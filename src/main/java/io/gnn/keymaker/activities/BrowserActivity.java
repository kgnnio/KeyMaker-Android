package io.gnn.keymaker.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.R;
import io.gnn.keymaker.models.Site;
import io.gnn.keymaker.utils.SitePasswordGenerator;

/**
 * Created by user on 2016/03/21.
 */
@DebugLog
public class BrowserActivity extends Activity {
    WebView mWebView;

    Site mSiteSelected;
    String mPassword;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        //
        mSiteSelected = (Site) getIntent().getSerializableExtra("site_selected");
        mPassword = SitePasswordGenerator.generateSitePassword(mSiteSelected);

        //
        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(mSiteSelected.getUri());

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(mWebView, url);

                String javaScript = "javascript: {" +
                        "document.getElementById('ap_email').value = '" + mSiteSelected.getUser() + "';" +
                        "document.getElementById('ap_password').value = '" + mPassword + "';" +
                        "var forms = document.getElementsByName('loginForm');" +
                        "forms.item(0).submit(); };";

                view.loadUrl(javaScript);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
