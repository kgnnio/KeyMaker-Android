package io.gnn.keymaker.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.KeyMakerApplication;
import io.gnn.keymaker.masterpassword.MasterPassword;
import io.gnn.keymaker.R;

/**
 * Created by owlowlgo on 2016/03/21.
 */
@DebugLog
public class GenerateMasterKeyActivity extends Activity {
    private ProgressDialog progressDialog;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_master_key);

        // Set up Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_generate_master_key);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
        toolbar.setTitle("Generate Master Key");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final EditText editTextFullName = (EditText) findViewById(R.id.edit_text_full_name);
        editTextFullName.setInputType(InputType.TYPE_CLASS_TEXT);
        final EditText editTextMasterPassword = (EditText) findViewById(R.id.edit_text_master_password);

        Button buttonToInputMasterPassword = (Button) findViewById(R.id.button_generate_master_key);
        buttonToInputMasterPassword.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Generate Master Key based on User name and Master Password.
                String fullName = editTextFullName.getText().toString();
                String masterPassword = editTextMasterPassword.getText().toString();

                // Kick thread to generate Master Key.
                new GenerateMasterKeyTask().execute(fullName, masterPassword);
            }
        });
    }

    //To use the AsyncTask, it must be subclassed
    private class GenerateMasterKeyTask extends AsyncTask<String, Integer, Void> {
        //Before running code in separate thread
        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(GenerateMasterKeyActivity.this, getString(R.string.progress_generate_master_key_header), getString(R.string.progress_generate_master_key_message), false, false);
        }

        // Generate Master Key in background.
        @Override
        protected Void doInBackground(String... params) {
            String fullName = params[0];
            String masterPassword = params[1];
            byte[] masterKey;

            synchronized (this) {
                // Generate Master Key.
                MasterPassword mp = new MasterPassword();
                mp.generateKey(fullName, masterPassword);
                masterKey = mp.getMasterKey();

                // Save Master Key to disk
                Gson gson = new Gson();
                String json = gson.toJson(masterKey);
                SharedPreferences seed = getSharedPreferences("seed", Context.MODE_PRIVATE);
                seed.edit().putString("seed", json).commit();
            }

            return null;
        }

        //Update the progress
        @Override
        protected void onProgressUpdate(Integer... values) {
            //set the current progress of the progress dialog
            progressDialog.setProgress(values[0]);
        }

        // Notify end of Master Key generation and return to MainActivity.
        @Override
        protected void onPostExecute(Void result) {
            // Close the progress dialog.
            progressDialog.dismiss();

            // Re-initialize the View.
            setContentView(R.layout.activity_generate_master_key);

            // Notify that the Master Key has been generated and saved to disk.
            Toast.makeText(GenerateMasterKeyActivity.this, R.string.toast_master_key_generated, Toast.LENGTH_LONG).show();

            // Return to Main Activity
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
