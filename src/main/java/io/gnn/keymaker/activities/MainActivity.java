package io.gnn.keymaker.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.common.primitives.UnsignedInteger;

import java.util.Set;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.KeyMakerApplication;
import io.gnn.keymaker.R;
import io.gnn.keymaker.fragments.FragmentListSites;
import io.gnn.keymaker.fragments.FragmentListTabs;
import io.gnn.keymaker.masterpassword.PasswordType;
import io.gnn.keymaker.models.Site;

/**
 *
 */
@DebugLog
public class MainActivity extends AppCompatActivity {

    public static MainActivity sInstance;

    private FragmentListSites mFragmentListSites;
    private FragmentListTabs mFragmentListTags;
    private TabLayout mTabLayout;

    public static MainActivity getInstance() {
        return sInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        sInstance = this;

        // Set up Status Bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.primary_dark));
        }

        // Set up Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setNavigationIcon(R.drawable.ic_menu_24dp);
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.action_to_generate_master_key) {
                    Intent intent = new Intent(MainActivity.this, GenerateMasterKeyActivity.class);
                    intent.setClassName("io.gnn.keymaker", "io.gnn.keymaker.activities.GenerateMasterKeyActivity");
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else if (id == R.id.menu_item_new_thingy) {
                    Intent intent = new Intent(MainActivity.this, PreferencesActivity.class);
                    intent.setClassName("io.gnn.keymaker", "io.gnn.keymaker.activities.PreferencesActivity");
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
                }
                return true;
            }
        });

        FloatingActionButton FAB = (FloatingActionButton) findViewById(R.id.fab_add_site);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int siteSelectedPosition = 0;
                Site siteSelected = new Site("", "", PasswordType.LONG_PASSWORD, UnsignedInteger.valueOf(1L), "", "", "");

                Intent intent = new Intent(KeyMakerApplication.getInstance().getApplicationContext(), SiteActivity.class);
                intent.putExtra("site_selected_position", siteSelectedPosition);
                intent.putExtra("site_selected", siteSelected);
                intent.putExtra("site_selected_is_new", true);
                intent.setClassName("io.gnn.keymaker", "io.gnn.keymaker.activities.SiteActivity");

                startActivity(intent);
            }
        });

        // Setup Tabs and content
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        bindWidgetsWithAnEvent();

        mFragmentListSites = new FragmentListSites();
        mFragmentListTags = new FragmentListTabs();

        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.tab_list_sites), true);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.tab_list_tabs));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFragmentListSites.update();
    }

    private void bindWidgetsWithAnEvent() {
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                //replaceFragment(mFragmentListSites);
                getSupportFragmentManager().beginTransaction().detach(mFragmentListTags).replace(R.id.frame_container, mFragmentListSites).attach(mFragmentListSites).addToBackStack(null).commit();
                break;
            case 1:
                //replaceFragment(mFragmentListTags);
                getSupportFragmentManager().beginTransaction().detach(mFragmentListSites).replace(R.id.frame_container, mFragmentListTags).attach(mFragmentListTags).addToBackStack(null).commit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
