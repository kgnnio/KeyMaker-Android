package io.gnn.keymaker.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import io.gnn.keymaker.R;

/**
 * Created by user on 2016/04/16.
 */
public class PreferencesActivity extends AppCompatActivity {

    public static final String KEY_SYNC_DROPBOX_ONOFF = "switch_dropbox_sync_onoff";
    public static final String KEY_SYNC_DROPBOX_SYNC_FREQUENCY = "list_dropbox_sync_frequency";

    public static final Boolean KEY_SYNC_DROPBOX_ONOFF_DEFAULT = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PrefsFragment())
                .commit();
    }


    public static class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
        SwitchPreference switchPreferenceDropboxSyncOnOff;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preference);

            //
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());

            switchPreferenceDropboxSyncOnOff = (SwitchPreference) getPreferenceScreen().findPreference(KEY_SYNC_DROPBOX_ONOFF);
            switchPreferenceDropboxSyncOnOff.setChecked(pref.getBoolean(KEY_SYNC_DROPBOX_ONOFF, KEY_SYNC_DROPBOX_ONOFF_DEFAULT));
            switchPreferenceDropboxSyncOnOff.setSummary(switchPreferenceDropboxSyncOnOff.isChecked() ? "ON" : "OFF");
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            change_pref();
        }

        private void change_pref() {

            switchPreferenceDropboxSyncOnOff.setSummary(switchPreferenceDropboxSyncOnOff.isChecked() ? "ON" : "OFF");

        }
    }

}
