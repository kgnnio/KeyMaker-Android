package io.gnn.keymaker.activities;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.primitives.UnsignedInteger;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.text.Format;
import java.text.MessageFormat;
import java.util.ArrayList;

import fr.ganfra.materialspinner.MaterialSpinner;
import hugo.weaving.DebugLog;
import io.gnn.keymaker.KeyMakerApplication;
import io.gnn.keymaker.masterpassword.PasswordType;
import io.gnn.keymaker.R;
import io.gnn.keymaker.models.Site;
import io.gnn.keymaker.utils.SitePasswordGenerator;

/**
 * Created by owlowlgo on 2016/03/21.
 */
@DebugLog
public class SiteActivity extends Activity {
    int mSiteSelectedPosition;
    Site mSiteSelected;
    boolean mSiteSelectedIsNew;

    EditText mEditTextName;
    EditText mEditTextUser;
    EditText mEditTextPassword;
    MaterialSpinner mMaterialSpinnerPasswordType;
    EditText mEditTextCounter;

    EditText mEditTextUri;
    EditText mEditTextIdUser;
    EditText mEditTextIdPassword;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site);

        // Set up Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_site);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbar.inflateMenu(R.menu.menu_site);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.action_delete) {
                    //
                    ArrayList<Site> arrayListSites = SiteActivity.loadSitesFromDisk();

                    // Remove currently selected Site and save to Shared Preferences if not new.
                    if (!mSiteSelectedIsNew) {
                        arrayListSites.remove(mSiteSelectedPosition);
                        saveSitesToSharedPreferences(arrayListSites);
                    }

                    // Notify that Site info has been deleted.
                    Toast.makeText(KeyMakerApplication.getInstance(), R.string.toast_site_has_been_deleted, Toast.LENGTH_LONG).show();

                    finish();
                }
                if (id == R.id.action_save) {
                    //
                    ArrayList<Site> arrayListSites = SiteActivity.loadSitesFromDisk();

                    // Remove currently selected Site if not new so that only the last edited version of the Site remains.
                    if (!mSiteSelectedIsNew)
                        arrayListSites.remove(mSiteSelectedPosition);

                    // Add updated version of Site.
                    Site siteUpdated = new Site(mEditTextName.getText().toString(), mEditTextUser.getText().toString(), PasswordType.values()[mMaterialSpinnerPasswordType.getSelectedItemPosition() - 1], UnsignedInteger.valueOf(mEditTextCounter.getText().toString()), mEditTextUri.getText().toString(), mEditTextIdUser.getText().toString(), mEditTextIdPassword.getText().toString());

                    arrayListSites.add(siteUpdated);
                    mSiteSelected = siteUpdated;

                    // Save Sites to disk.
                    saveSitesToSharedPreferences(arrayListSites);

                    //

                    // Notify that Site info has been updated.
                    Toast.makeText(SiteActivity.this, R.string.toast_site_has_been_activated, Toast.LENGTH_LONG).show();

                    //prepareIntent(arrayListSites);
                    finish();
                }
                return true;
            }
        });

        //
        mSiteSelected = (Site) getIntent().getSerializableExtra("site_selected");
        mSiteSelectedPosition = (int) getIntent().getSerializableExtra("site_selected_position");
        mSiteSelectedIsNew = (boolean) getIntent().getSerializableExtra("site_selected_is_new");

        //
        TextView textViewSiteName = (TextView) findViewById(R.id.text_view_site_name);
        textViewSiteName.setText(mSiteSelected.getName());
        mEditTextName = (EditText) findViewById(R.id.editTextSite);
        mEditTextName.setText(mSiteSelected.getName());
        mEditTextUser = (EditText) findViewById(R.id.edit_text_user);
        mEditTextUser.setText(mSiteSelected.getUser());
        mEditTextPassword = (EditText) findViewById(R.id.edit_text_password);
        mEditTextPassword.setKeyListener(null);
        mMaterialSpinnerPasswordType = (MaterialSpinner) findViewById(R.id.spinnerSitePasswordType);
        mMaterialSpinnerPasswordType.setSelection(mSiteSelected.getPasswordType().ordinal() + 1);

        mEditTextCounter = (EditText) findViewById(R.id.edit_text_counter);
        mEditTextCounter.setText(mSiteSelected.getCounter().toString());

        mEditTextUri = (EditText) findViewById(R.id.edit_text_uri);
        mEditTextUri.setText(mSiteSelected.getUri());
        mEditTextIdUser = (EditText) findViewById(R.id.edit_text_id_user);;
        mEditTextIdUser.setText(mSiteSelected.getIdUser());;
        mEditTextIdPassword = (EditText) findViewById(R.id.edit_text_id_password);;
        mEditTextIdPassword.setText(mSiteSelected.getIdPassword());;

        ArrayAdapter arrayAdapterSitePasswordTypes = new ArrayAdapter(this, android.R.layout.simple_spinner_item);

        Format formatPasswordTypes = new MessageFormat(getResources().getString(R.string.passwordtypes));
        for (PasswordType passwordType : PasswordType.values()) {
            arrayAdapterSitePasswordTypes.add(formatPasswordTypes.format(new Object[] { passwordType.ordinal() }));
        }
        arrayAdapterSitePasswordTypes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mMaterialSpinnerPasswordType = (MaterialSpinner) findViewById(R.id.spinnerSitePasswordType);
        mMaterialSpinnerPasswordType.setAdapter(arrayAdapterSitePasswordTypes);

        FloatingActionButton floatingButtonLogin = (FloatingActionButton) findViewById(R.id.fabLogin);
        floatingButtonLogin.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    toBrowserActivity(mSiteSelected);

                    finish();
                } catch (IllegalArgumentException e) {
                    // Notify user that the Master Key is not generated.
                    Toast.makeText(SiteActivity.this, R.string.toast_master_key_has_not_been_generated, Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });

        ImageButton buttonImageCopyName = (ImageButton) findViewById(R.id.image_button_copy_user);
        buttonImageCopyName.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        ImageButton buttonImageCopyPassword = (ImageButton) findViewById(R.id.image_button_copy_password);
        buttonImageCopyPassword.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        ImageButton buttonSiteCopyUser = (ImageButton) findViewById(R.id.image_button_copy_user);
        buttonSiteCopyUser.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Get Site User.
                final String siteUser = ((EditText) findViewById(R.id.edit_text_user)).getText().toString();

                ClipData.Item item = new ClipData.Item(siteUser);
                String[] mimeType = new String[1];
                mimeType[0] = ClipDescription.MIMETYPE_TEXT_PLAIN;
                ClipData clipData = new ClipData(new ClipDescription("site_user", mimeType), item);

                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setPrimaryClip(clipData);

                // Notify that the operation has been performed.
                Toast.makeText(SiteActivity.this, R.string.toast_site_user_has_been_copied_to_clipboard, Toast.LENGTH_LONG).show();
            }
        });

        ImageButton buttonSiteCopyPassword = (ImageButton) findViewById(R.id.image_button_copy_password);
        buttonSiteCopyPassword.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Generate Site Password.
                Site siteUpdated = new Site(mEditTextName.getText().toString(), mEditTextUser.getText().toString(), PasswordType.values()[mMaterialSpinnerPasswordType.getSelectedItemPosition() - 1], UnsignedInteger.valueOf(mEditTextCounter.getText().toString()), mEditTextUri.getText().toString(), mEditTextIdUser.getText().toString(), mEditTextIdPassword.getText().toString());
                mSiteSelected = siteUpdated;

                String sitePassword = SitePasswordGenerator.generateSitePassword(mSiteSelected);

                // Copy Site Password to the clipboard.
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("site_password", sitePassword);
                clipboard.setPrimaryClip(clip);

                // Notify that the operation has been performed.
                Toast.makeText(SiteActivity.this, R.string.toast_site_password_has_been_generated_and_copied_to_clipboard, Toast.LENGTH_LONG).show();
            }
        });

        ImageButton buttonSiteCounterAdd = (ImageButton) findViewById(R.id.image_button_counter_add);
        buttonSiteCounterAdd.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                UnsignedInteger counter = UnsignedInteger.valueOf(mEditTextCounter.getText().toString());
                if (counter.compareTo(UnsignedInteger.MAX_VALUE) != 0) {
                    counter = counter.plus(UnsignedInteger.ONE);
                    mEditTextCounter.setText(counter.toString());
                }
            }
        });

        ImageButton buttonSiteCounterSubtract = (ImageButton) findViewById(R.id.image_button_counter_subtract);
        buttonSiteCounterSubtract.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                UnsignedInteger counter = UnsignedInteger.valueOf(mEditTextCounter.getText().toString());
                if (counter.compareTo(UnsignedInteger.ONE) != 0) {
                    counter = counter.minus(UnsignedInteger.ONE);
                    mEditTextCounter.setText(counter.toString());
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // いつものUPナビゲーションの処理
        switch (id) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
      Prepare updated version of list of Sites in intent.
     */
    private void prepareIntent(ArrayList<Site> arrayListSites) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        intent.putExtra("site_list", arrayListSites);
        intent.putExtras(bundle);
        setResult(1, intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void toBrowserActivity(Site siteSelected) {
        // Notify user of password.
        Toast.makeText(SiteActivity.this, R.string.toast_password_has_been_generated, Toast.LENGTH_LONG).show();

        // Launch Internal Browser and pass login details
        Intent intent = new Intent();
        intent.putExtra("site_selected", siteSelected);
        //intent.putExtra("site_password", sitePassword);
        intent.setClassName("io.gnn.keymaker", "io.gnn.keymaker.activities.BrowserActivity");
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /*
      Save Sites to Shared Preferences.
    */
    static public void saveSitesToSharedPreferences(ArrayList<Site> arrayListSites) {
        Gson gson = new Gson();
        String json = gson.toJson(arrayListSites);
        SharedPreferences seed = KeyMakerApplication.getInstance().getApplicationContext().getSharedPreferences("site_list", Context.MODE_PRIVATE);
        seed.edit().putString("site_list", json).commit();
    }

    static public ArrayList<Site> loadSitesFromDisk() {
        ArrayList<Site> arrayListSites;

        SharedPreferences pref = KeyMakerApplication.getInstance().getApplicationContext().getSharedPreferences("site_list", Context.MODE_PRIVATE);
        String json = pref.getString("site_list", "");
        Gson gson = new Gson();
        arrayListSites = gson.fromJson(json, new TypeToken<ArrayList<Site>>() {
        }.getType());

        return arrayListSites;
    }

    static public String loadSitesFromDiskAsJson() {
        SharedPreferences pref = KeyMakerApplication.getInstance().getApplicationContext().getSharedPreferences("site_list", Context.MODE_PRIVATE);
        String json = pref.getString("site_list", "");

        return json;
    }
}
