package io.gnn.keymaker.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.R;
import io.gnn.keymaker.models.Site;
import io.gnn.keymaker.utils.SitePasswordGenerator;

@DebugLog
public class ListViewAdapter extends BaseAdapter {

    private ArrayList<Site> mArrayListSites;
    private Context context;
    private LayoutInflater inflater;

    public ListViewAdapter(Context context, ArrayList<Site> mArrayListSites) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mArrayListSites = mArrayListSites;
    }

    @Override
    public int getCount() {
        return mArrayListSites.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayListSites.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final SiteHolder holder;
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item_site, parent, false);
            holder = new SiteHolder();
            assert view != null;
            holder.textViewSiteName = (TextView) view.findViewById(R.id.text_site_Name);
            holder.textViewSiteUser = (TextView) view.findViewById(R.id.text_site_user);
            holder.imageButtonOpenInBrowser = (ImageButton) view.findViewById(R.id.image_button_open_in_browser);
            holder.imageButtonOpenInBrowser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(KeyMakerApplication.getInstance(), "OK", Toast.LENGTH_LONG).show();
                    Site siteSelected = mArrayListSites.get(position);

                    // Launch Internal Browser and pass login details
                    Intent intent = new Intent();
                    intent.putExtra("site_selected", siteSelected);
                    intent.setClassName("io.gnn.keymaker", "io.gnn.keymaker.activities.BrowserActivity");

                    Activity activity = (Activity) context;
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            });

            view.setTag(holder);
        } else {
            holder = (SiteHolder) view.getTag();
        }

        holder.textViewSiteName.setText(mArrayListSites.get(position).getName());
        holder.textViewSiteUser.setText(mArrayListSites.get(position).getUser());

        return view;
    }

    class SiteHolder {
        TextView textViewSiteName;
        TextView textViewSiteUser;
        ImageButton imageButtonOpenInBrowser;
    }

}

