package io.gnn.keymaker.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.common.primitives.UnsignedInteger;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.KeyMakerApplication;
import io.gnn.keymaker.R;
import io.gnn.keymaker.activities.MainActivity;
import io.gnn.keymaker.activities.SiteActivity;
import io.gnn.keymaker.adapters.ListViewAdapter;
import io.gnn.keymaker.masterpassword.PasswordType;
import io.gnn.keymaker.models.Site;

import java.util.ArrayList;

@DebugLog
public class FragmentListSites extends Fragment {
    private ListView listView;
    private ArrayList<Site> mArrayListSites;

    static int REQUEST_SITE_UPDATE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Load Meta Data from Disk.
        mArrayListSites = new ArrayList<>();

        if (mArrayListSites.isEmpty()) {
            mArrayListSites = SiteActivity.loadSitesFromDisk();
        }

        if (mArrayListSites == null) {
            mArrayListSites = new ArrayList<>();
            mArrayListSites.add(new Site("amazon.co.jp", "k@gnn.io", PasswordType.LONG_PASSWORD, UnsignedInteger.valueOf(1), "https://www.amazon.co.jp/ap/signin?_encoding=UTF8&openid.assoc_handle=jpflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.co.jp%2F%3Fref_%3Dnav_ya_signin", "ap_email", "ap_password"));
            mArrayListSites.add(new Site("yahoo.co.jp", "kgnnio", PasswordType.LONG_PASSWORD, UnsignedInteger.valueOf(1), "", "", ""));
            mArrayListSites.add(new Site("yahoo2.co.jp", "kgnnio", PasswordType.LONG_PASSWORD, UnsignedInteger.valueOf(1), "", "", ""));
            mArrayListSites.add(new Site("yahoo3.co.jp", "kgnnio", PasswordType.LONG_PASSWORD, UnsignedInteger.valueOf(1), "", "", ""));
            mArrayListSites.add(new Site("yahoo4.co.jp", "kgnnio", PasswordType.LONG_PASSWORD, UnsignedInteger.valueOf(1), "", "", ""));

            SiteActivity.saveSitesToSharedPreferences(mArrayListSites);
        }

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_list_sites, null);
        getAllWidgets(rootView);
        setAdapter();

        //
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int siteSelectedPosition = position;
                Site siteSelected = mArrayListSites.get(position);

                Intent intent = new Intent(KeyMakerApplication.getInstance().getApplicationContext(), SiteActivity.class);
                intent.putExtra("site_selected_position", siteSelectedPosition);
                intent.putExtra("site_selected", siteSelected);
                intent.putExtra("site_selected_is_new", false);
                intent.setClassName("io.gnn.keymaker", "io.gnn.keymaker.activities.SiteActivity");

                startActivityForResult(intent, REQUEST_SITE_UPDATE);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        return rootView;
    }

    private void getAllWidgets(View view) {
        listView = (ListView) view.findViewById(R.id.list_view_sites);
    }

    private void setAdapter() {
        ListViewAdapter listViewAdapter = new ListViewAdapter(MainActivity.getInstance(), mArrayListSites);
        listView.setAdapter(listViewAdapter);
    }

    public void update() {
        mArrayListSites = SiteActivity.loadSitesFromDisk();
        ListViewAdapter listViewAdapter = new ListViewAdapter(MainActivity.getInstance(), mArrayListSites);
        listView.setAdapter(listViewAdapter);
    }
}
