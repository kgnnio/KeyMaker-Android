package io.gnn.keymaker.fragments;

import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.R;
import io.gnn.keymaker.activities.MainActivity;
import io.gnn.keymaker.adapters.ListViewAdapter;
import io.gnn.keymaker.models.Site;


import java.util.ArrayList;

@DebugLog
public class FragmentListTabs extends Fragment {
    private ListView listView;
    ArrayList mArrayListTags = new ArrayList();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_list_tags, null);
        getAllWidgets(rootView);
        setAdapter();
        return rootView;
    }

    private void getAllWidgets(View view) {
        listView = (ListView) view.findViewById(R.id.list_view_tags);
    }

    private void setAdapter() {
        ListViewAdapter listViewAdapter = new ListViewAdapter(MainActivity.getInstance(), mArrayListTags);
        listView.setAdapter(listViewAdapter);
    }
}
