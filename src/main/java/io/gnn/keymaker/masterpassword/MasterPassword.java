package io.gnn.keymaker.masterpassword;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Charsets;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.io.ByteProcessor;
import com.google.common.io.ByteSource;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.UnsignedInteger;

import org.spongycastle.crypto.generators.SCrypt;
import org.spongycastle.util.Arrays;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.models.Site;

/**
 * Created by owlowlgo.
 */
@DebugLog
public class MasterPassword {
    // Scrypt related activity_settings.
    protected static final int MP_N = 32768;                                  // CPU/Memory cost parameter. Must be larger than 1, a power of 2 and less than <code>2^(128 * r / 8)</code>.
    protected static final int MP_r = 8;                                      // The block size, must be >= 1.
    protected static final int MP_p = 2;                                      // Parallelization parameter. Must be a positive integer less than or equal to <code>Integer.MAX_VALUE / (128 * r * 8)</code>.
    protected static final int MP_dkLen = 64;                                 // The length of the masterKey to generate.
    protected static final int MP_intLen = 32;                                // Length of Int
    protected static final Charset MP_charset = Charsets.UTF_8;
    protected static final ByteOrder MP_byteOrder = ByteOrder.BIG_ENDIAN;
    protected static final String MP_keyScope = "com.lyndir.masterpassword";

    // List of templates strings mapped by masterPassword type
    ListMultimap<PasswordType, String> passwordTypeTemplateStringMap = ArrayListMultimap.create();

    // Character Group Map.
    Map<Character, String> charGroupMap = new HashMap<>();

    private byte[] masterKey;

    public MasterPassword() {
        this.charGroupMap.put('V', "AEIOU");
        this.charGroupMap.put('C', "BCDFGHJKLMNPQRSTVWXYZ");
        this.charGroupMap.put('v', "aeiou");
        this.charGroupMap.put('c', "bcdfghjklmnpqrstvwxyz");
        this.charGroupMap.put('A', "AEIOUBCDFGHJKLMNPQRSTVWXYZ");
        this.charGroupMap.put('a', "AEIOUaeiouBCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz");
        this.charGroupMap.put('n', "0123456789");
        this.charGroupMap.put('o', "@&%?,=[]_:-+*$#!'^~;()/.");
        this.charGroupMap.put('x', "AEIOUaeiouBCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz0123456789!@#$%^&*()");
        this.charGroupMap.put(' ', " ");

        // Password templates
        this.passwordTypeTemplateStringMap.put(PasswordType.MAXIMUM_SECURITY_PASSWORD, "anoxxxxxxxxxxxxxxxxx");
        this.passwordTypeTemplateStringMap.put(PasswordType.MAXIMUM_SECURITY_PASSWORD, "axxxxxxxxxxxxxxxxxno");

        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvnoCvcvCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvcvnoCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvcvCvcvno");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccnoCvcvCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccCvcvnoCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccCvcvCvcvno");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvnoCvccCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvccnoCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvccCvcvno");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvnoCvcvCvcc");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvcvnoCvcc");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvcvCvccno");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccnoCvccCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccCvccnoCvcv");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccCvccCvcvno");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvnoCvccCvcc");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvccnoCvcc");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvcvCvccCvccno");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccnoCvcvCvcc");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccCvcvnoCvcc");
        this.passwordTypeTemplateStringMap.put(PasswordType.LONG_PASSWORD, "CvccCvcvCvccno");

        this.passwordTypeTemplateStringMap.put(PasswordType.MEDIUM_PASSWORD, "CvcnoCvc");
        this.passwordTypeTemplateStringMap.put(PasswordType.MEDIUM_PASSWORD, "CvcCvcno");

        this.passwordTypeTemplateStringMap.put(PasswordType.SHORT_PASSWORD, "Cvcn");

        this.passwordTypeTemplateStringMap.put(PasswordType.BASIC_PASSWORD, "aaanaaan");
        this.passwordTypeTemplateStringMap.put(PasswordType.BASIC_PASSWORD, "aannaaan");
        this.passwordTypeTemplateStringMap.put(PasswordType.BASIC_PASSWORD, "aaannaaa");

        this.passwordTypeTemplateStringMap.put(PasswordType.PIN, "nnnn");
    }

    public byte[] getMasterKey() {
        return this.masterKey;
    }

    public void setMasterKey(byte[] masterKey) {
        this.masterKey = masterKey;
    }

    public void generateKey(final String userName, final String masterPassword) {
        this.masterKey = deriveKey(userName, masterPassword.toCharArray());
    }

    @Nullable
    protected byte[] deriveKey(String userName, final char[] password) {
        byte[] userNameBytes = userName.getBytes(MP_charset);
        byte[] userNameLengthBytes = bytesForInt(userName.length());

        byte[] masterKeySalt = Bytes.concat(MP_keyScope.getBytes(MP_charset), userNameLengthBytes, userNameBytes);

        ByteBuffer mpBytesBuf = MP_charset.encode(CharBuffer.wrap(password));
        byte[] mpBytes = new byte[mpBytesBuf.remaining()];
        mpBytesBuf.get(mpBytes, 0, mpBytes.length);
        Arrays.fill(mpBytesBuf.array(), (byte) 0);

        return scrypt(masterKeySalt, mpBytes);
    }

    @NonNull
    public byte[] getSeed(Site site) {
        String siteName = site.getName();
        UnsignedInteger siteCounter = site.getCounter();

        String siteScope = MP_keyScope;
        byte[] siteNameBytes = siteName.getBytes(MP_charset);
        byte[] siteNameLengthBytes = bytesForInt(siteNameBytes.length);
        byte[] siteCounterBytes = bytesForInt(siteCounter);

        byte[] sitePasswordInfo = Bytes.concat(siteScope.getBytes(MP_charset), siteNameLengthBytes, siteNameBytes, siteCounterBytes);
        byte[] sitePasswordSeed = generateHMAC(this.masterKey, sitePasswordInfo);

        return sitePasswordSeed;
    }

    private byte[] generateHMAC(final byte[] key, final byte[] source) {
        final ByteSource sourceByteSource = ByteSource.wrap(source);

        final Mac mac;
        try {
            mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(key, mac.getAlgorithm()));
            try {
                return sourceByteSource.read(new ByteProcessor<byte[]>() {
                    @Override
                    public boolean processBytes(final byte[] buf, final int off, final int len) {
                        mac.update(buf, off, len);
                        return true;
                    }

                    @Override
                    public byte[] getResult() {
                        return mac.doFinal();
                    }
                });
            } catch (final IOException e) {
                System.out.println(e);
            }
        } catch (final NoSuchAlgorithmException e) {
            System.out.println(e);
        } catch (final InvalidKeyException e) {
            System.out.println(e);
        }

        return null;
    }

    public String generateSitePassword(final Site site, final byte[] seed) {

        int templateIndex = seed[0] & 0xFF;

        List<String> passwordTypeTemplateStringList = this.passwordTypeTemplateStringMap.get(site.getPasswordType());
        String template = passwordTypeTemplateStringList.get(templateIndex % passwordTypeTemplateStringList.size());

        StringBuilder sitePassword = new StringBuilder(template.length());
        for (int i = 0; i < template.length(); ++i) {
            char passwordCharacterType = template.charAt(i);
            char[] characters = this.charGroupMap.get(passwordCharacterType).toCharArray();

            templateIndex = Math.abs(seed[i + 1] & 0xFF);

            sitePassword.append(characters[templateIndex % characters.length]);
        }

        return new String(sitePassword);

    }

    protected byte[] scrypt(final byte[] masterKeySalt, final byte[] mpBytes) {
        try {
            return SCrypt.generate(mpBytes, masterKeySalt, MP_N, MP_r, MP_p, MP_dkLen);
        } finally {
            Arrays.fill(mpBytes, (byte) 0);
        }
    }

    protected byte[] bytesForInt(final int number) {
        return ByteBuffer.allocate(MP_intLen / Byte.SIZE).order(MP_byteOrder).putInt(number).array();
    }

    protected byte[] bytesForInt(final UnsignedInteger number) {
        return ByteBuffer.allocate(MP_intLen / Byte.SIZE).order(MP_byteOrder).putInt(number.intValue()).array();
    }
}
