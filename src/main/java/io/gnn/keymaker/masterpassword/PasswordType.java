package io.gnn.keymaker.masterpassword;

import hugo.weaving.DebugLog;

/**
 * Created by owlowlgo.
 */
@DebugLog
public enum PasswordType {
    MAXIMUM_SECURITY_PASSWORD(0),
    LONG_PASSWORD(1),
    MEDIUM_PASSWORD(2),
    SHORT_PASSWORD(3),
    BASIC_PASSWORD(4),
    PIN(5);

    private final int typeNo;

    PasswordType(int typeNo) {
        this.typeNo = typeNo;
    }

    public int getValue() {
        return typeNo;
    }
}
