package io.gnn.keymaker.models;

import com.google.common.primitives.UnsignedInteger;

import java.io.Serializable;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.masterpassword.PasswordType;

/**
 * Created by owlowlgo.
 */
@DebugLog
public class Site implements Serializable {
    private long id;

    private String name;
    private String user;
    private PasswordType passwordType;
    private UnsignedInteger counter;

    private String uri;
    private String idUser;
    private String idPassword;

    public Site(String name, String user, PasswordType passwordType, UnsignedInteger counter, String uri, String idUser, String idPassword) {
        this.name = name;
        this.user = user;
        this.passwordType = passwordType;
        this.counter = counter;

        this.uri = uri;
        this.idUser = idUser;
        this.idPassword = idPassword;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public PasswordType getPasswordType() {
        return passwordType;
    }

    public void setPasswordType(PasswordType passwordType) {
        this.passwordType = passwordType;
    }

    public UnsignedInteger getCounter() {
        return counter;
    }

    public void setCounter(UnsignedInteger counter) {
        this.counter = counter;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdPassword() {
        return idPassword;
    }

    public void setIdPassword(String idPassword) {
        this.idPassword = idPassword;
    }

}
