package io.gnn.keymaker.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AppKeyPair;

import hugo.weaving.DebugLog;

/**
 * Created by user on 2016/03/27.
 */
@DebugLog
public class DropboxUtils {
    private static final String TOKEN = "1BmhLD_S_bAAAAAAAAAACNrT5NF0KDIlE6fXHKaRcO8Zbd31JYnlqMVtBkDdiiN_";
    private static final String PREF_NAME = "dropbox";
    public static final String APPKEY = "e086um3xkqz6y50";
    public static final String APPKEYSECRET = "3l59jnobtedlhql";
    private Context context;

    public DropboxUtils(Context context) {
        this.context = context;
    }

    public void storeOauth2AccessToken(String secret){
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN, secret);
        editor.commit();
    }

    public AndroidAuthSession loadAndroidAuthSession() {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        String token = preferences.getString(TOKEN, null);
        if (token != null) {
            AppKeyPair appKeys = new AppKeyPair(APPKEY, APPKEYSECRET);
            return new AndroidAuthSession(appKeys,token);
        } else {

            return null;
        }
    }

    public boolean hasLoadAndroidAuthSession() {
        return loadAndroidAuthSession() != null;
    }

}
