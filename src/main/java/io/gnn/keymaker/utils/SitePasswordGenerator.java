package io.gnn.keymaker.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import hugo.weaving.DebugLog;
import io.gnn.keymaker.KeyMakerApplication;
import io.gnn.keymaker.masterpassword.MasterPassword;
import io.gnn.keymaker.models.Site;

/**
 * Created by user on 2016/03/26.
 */
@DebugLog
public class SitePasswordGenerator {
    static public String generateSitePassword(Site site) {
        // Load Master Key from disk.
        MasterPassword masterPassword = new MasterPassword();
        byte[] key;

        SharedPreferences pref = KeyMakerApplication.getInstance().getApplicationContext().getSharedPreferences("seed", Context.MODE_PRIVATE);
        String json = pref.getString("seed", "");
        Gson gson = new Gson();
        key = gson.fromJson(json, byte[].class);
        masterPassword.setMasterKey(key);

        // Generate Site Password.
        byte[] seed = masterPassword.getSeed(site);
        String sitePassword = masterPassword.generateSitePassword(site, seed);

        return sitePassword;
    }
}
