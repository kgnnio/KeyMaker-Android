package io.gnn.keymaker;

import com.google.common.primitives.UnsignedInteger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;

import io.gnn.keymaker.masterpassword.MasterPassword;
import io.gnn.keymaker.masterpassword.PasswordType;
import io.gnn.keymaker.models.Site;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
@RunWith(Parameterized.class)
public class MasterPasswordTest {

    private String user;
    private String masterPassword;
    private String siteName;
    private UnsignedInteger siteCounter;
    private PasswordType passwordType;
    private String expectedSitePassword;

    /**
     * @param user
     * @param masterPassword
     * @param siteName
     * @param siteCounter
     * @param passwordType
     * @param expectedSitePassword
     */
    public MasterPasswordTest(String user, String masterPassword, String siteName, UnsignedInteger siteCounter, PasswordType passwordType, String expectedSitePassword) {
        this.user = user;
        this.masterPassword = masterPassword;
        this.siteName = siteName;
        this.siteCounter = siteCounter;
        this.passwordType = passwordType;
        this.expectedSitePassword = expectedSitePassword;
    }

    /**
     * @return
     */
    @Parameters(name = "No. {index}: userName: {0}, masterPassword: {1}, siteName: {2}, siteCounter: {3}, passwordType: {4}, expects sitePassword: {5}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"Alice", "abcde", "apple", UnsignedInteger.valueOf(1), PasswordType.MAXIMUM_SECURITY_PASSWORD, "plMGXmBv7Kk^!H61PF9?"},
                {"Alice", "abcde", "apple", UnsignedInteger.valueOf(1), PasswordType.LONG_PASSWORD, "MakuKuke1^Huze"},
                {"Alice", "abcde", "apple", UnsignedInteger.valueOf(1), PasswordType.MEDIUM_PASSWORD, "MakSov4."},
                {"Alice", "abcde", "apple", UnsignedInteger.valueOf(1), PasswordType.SHORT_PASSWORD, "Mak4"},
                {"Alice", "abcde", "apple", UnsignedInteger.valueOf(1), PasswordType.BASIC_PASSWORD, "pv14XXy1"},
                {"Alice", "abcde", "apple", UnsignedInteger.valueOf(1), PasswordType.PIN, "8514"},

                {"Alice", "abcde", "test-äöøß", UnsignedInteger.valueOf(1), PasswordType.MAXIMUM_SECURITY_PASSWORD, "k2[utahMAhrPPA%#wZmv"},
                {"Alice", "abcde", "test-äöøß", UnsignedInteger.valueOf(1), PasswordType.LONG_PASSWORD, "WittRaqkJoda7@"},
                {"Alice", "abcde", "test-äöøß", UnsignedInteger.valueOf(1), PasswordType.MEDIUM_PASSWORD, "Wit5/Hak"},
                {"Alice", "abcde", "test-äöøß", UnsignedInteger.valueOf(1), PasswordType.SHORT_PASSWORD, "Wit5"},
                {"Alice", "abcde", "test-äöøß", UnsignedInteger.valueOf(1), PasswordType.BASIC_PASSWORD, "kLk58aSl"},
                {"Alice", "abcde", "test-äöøß", UnsignedInteger.valueOf(1), PasswordType.PIN, "8265"},

                {"test-user", "test-mp", "test-activity_site", UnsignedInteger.valueOf(42), PasswordType.MAXIMUM_SECURITY_PASSWORD, "IcHQ@n&tHGV2zaHkK08;"},
                {"test-user", "test-mp", "test-activity_site", UnsignedInteger.valueOf(42), PasswordType.LONG_PASSWORD, "QefyMase1#Pemh"},
                {"test-user", "test-mp", "test-activity_site", UnsignedInteger.valueOf(42), PasswordType.MEDIUM_PASSWORD, "QefYaw0/"},
                {"test-user", "test-mp", "test-activity_site", UnsignedInteger.valueOf(42), PasswordType.SHORT_PASSWORD, "Qef6"},
                {"test-user", "test-mp", "test-activity_site", UnsignedInteger.valueOf(42), PasswordType.BASIC_PASSWORD, "IN76bYh6"},
                {"test-user", "test-mp", "test-activity_site", UnsignedInteger.valueOf(42), PasswordType.PIN, "4676"},

                {"Robert Lee Mitchell", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.MAXIMUM_SECURITY_PASSWORD, "W6@692^B1#&@gVdSdLZ@"},
                {"Robert Lee Mitchell", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.LONG_PASSWORD, "Jejr5[RepuSosp"},
                {"Robert Lee Mitchell", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.MEDIUM_PASSWORD, "Jej2$Quv"},
                {"Robert Lee Mitchell", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.SHORT_PASSWORD, "Jej2"},
                {"Robert Lee Mitchell", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.BASIC_PASSWORD, "WAo2xIg6"},
                {"Robert Lee Mitchell", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.PIN, "7662"},

                {"Robert Lee Mitchell", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(4294967295L), PasswordType.LONG_PASSWORD, "XambHoqo6[Peni"},

                {"⛄", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.MAXIMUM_SECURITY_PASSWORD, "ADgB4R*N#aHV35aON32@"},
                {"⛄", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.LONG_PASSWORD, "NopaDajh8=Fene"},
                {"⛄", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.MEDIUM_PASSWORD, "NopNop9("},
                {"⛄", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.SHORT_PASSWORD, "Nop0"},
                {"⛄", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.BASIC_PASSWORD, "AN90SqK6"},
                {"⛄", "banana colored duckling", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.PIN, "2890"},

                {"Robert Lee Mitchell", "⛄", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.MAXIMUM_SECURITY_PASSWORD, "S1(loFtbs6zjGn)5yP(S"},
                {"Robert Lee Mitchell", "⛄", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.LONG_PASSWORD, "QesuHirv5-Xepl"},
                {"Robert Lee Mitchell", "⛄", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.MEDIUM_PASSWORD, "Qes9_Nov"},
                {"Robert Lee Mitchell", "⛄", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.SHORT_PASSWORD, "Qes9"},
                {"Robert Lee Mitchell", "⛄", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.BASIC_PASSWORD, "Ss09wEG7"},
                {"Robert Lee Mitchell", "⛄", "masterpasswordapp.com", UnsignedInteger.valueOf(1), PasswordType.PIN, "0109"},

                {"Robert Lee Mitchell", "banana colored duckling", "⛄", UnsignedInteger.valueOf(1), PasswordType.MAXIMUM_SECURITY_PASSWORD, "bp7rJKc7kaXc4sxOwG0*"},
                {"Robert Lee Mitchell", "banana colored duckling", "⛄", UnsignedInteger.valueOf(1), PasswordType.LONG_PASSWORD, "LiheCuwhSerz6)"},
                {"Robert Lee Mitchell", "banana colored duckling", "⛄", UnsignedInteger.valueOf(1), PasswordType.MEDIUM_PASSWORD, "LihPih8+"},
                {"Robert Lee Mitchell", "banana colored duckling", "⛄", UnsignedInteger.valueOf(1), PasswordType.SHORT_PASSWORD, "Lih6"},
                {"Robert Lee Mitchell", "banana colored duckling", "⛄", UnsignedInteger.valueOf(1), PasswordType.BASIC_PASSWORD, "bpW62jmW"},
                {"Robert Lee Mitchell", "banana colored duckling", "⛄", UnsignedInteger.valueOf(1), PasswordType.PIN, "9216"}

        });
    }

    /**
     *
     */
    @Test
    public void testGetSitePassword() {
        assertThat(getSitePassword(this.user, this.masterPassword, this.siteName, this.siteCounter, this.passwordType), is(this.expectedSitePassword));
    }

    private String getSitePassword(String user, String password, String siteName, UnsignedInteger siteCounter, PasswordType passwordType) {
        MasterPassword masterPassword = new MasterPassword();
        masterPassword.generateKey(user, password);

        Site site = new Site(siteName, "", passwordType, siteCounter, "", "", "");
        byte[] seed = masterPassword.getSeed(site);

        String sitePassword = masterPassword.generateSitePassword(site, seed);

        return sitePassword;
    }

}